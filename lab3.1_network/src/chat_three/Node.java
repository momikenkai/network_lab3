package chat_three;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class Node {
    private String nodeName;
    private int percent;
    private InetAddress parentIP;
    private int parentPort;
    private boolean root;
    private ArrayList<Message> messages;
    private ArrayList<IpPortWrapper> children;
    private DatagramSocket socket;
    private boolean end;
    Node(String[] args) throws NumberFormatException, SocketException, UnknownHostException {
        end = false;
        root = true;
        nodeName = args[0];
        percent = Integer.valueOf(args[1]);
        int nodePort = Integer.valueOf(args[2]);
        if (args.length == 5){
            root = false;
            parentIP = InetAddress.getByName(args[3]);
            parentPort = Integer.valueOf(args[4]);
        }
        messages = new ArrayList<>();
        children = new ArrayList<>();
        socket = new DatagramSocket(nodePort);
    }

    void addMessage(Message message, boolean node){
        boolean contains = false;
        for (Message message1 : messages){
            if(message1.equals(message)){
                contains = true;
            }
        }
        if (!contains) {
            messages.add(message);
            if(!node) {
                System.out.println(message.getMessage() + "\n");
            }
        }
    }

    ArrayList<IpPortWrapper> getChildren(){
        return children;
    }

    void addChild(InetAddress inetAddress, int port){
        children.add(new IpPortWrapper(inetAddress, port));
    }

    boolean isRoot(){
        return root;
    }

    String getNodeName() {
        return nodeName;
    }

    public DatagramSocket getSocket() {
        return socket;
    }

    public int percent() {
        return percent;
    }

    InetAddress getParentIP() {
        return parentIP;
    }

    int getParentPort() {
        return parentPort;
    }

    public void printNodeMessages(){
        messages.forEach(message -> System.out.println(message.getMessage() + "\n"));
    }


    public void breakRelation(InetAddress inetAddress, int port){
        if(inetAddress.equals(parentIP) && port == parentPort){
            root = true;
            System.out.println("Connection lost with " + port + '\n' + "Я батя теперь" + '\n');
            return;
        }
        for(IpPortWrapper ipPortWrapper: children){
            if(ipPortWrapper.getInetAddress().equals(inetAddress) && ipPortWrapper.getPort() == port){
                children.remove(ipPortWrapper);
                System.out.println("Connection lost with " + port + '\n');
                break;
            }
        }
    }

    void setEnd(){
        end = true;
    }

    boolean isEnd(){
        return end;
    }
}
