package chat_three;

import java.net.InetAddress;

public class IpPortWrapper {
    private InetAddress inetAddress;
    private Integer port;
    private int numOfResend;

    public IpPortWrapper(InetAddress inetAddress, Integer port){
        this.inetAddress = inetAddress;
        this.port = port;
        numOfResend = 0;
    }

    public void incrementResend(){
        numOfResend++;
    }

    public int getNumOfResend(){
        return numOfResend;
    }

    public void resetNumOfResend(){
        numOfResend = 0;
    }

    public InetAddress getInetAddress() {
        return inetAddress;
    }

    public Integer getPort() {
        return port;
    }
}
