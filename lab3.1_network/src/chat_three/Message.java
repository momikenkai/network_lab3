package chat_three;

import java.io.Serializable;

public class Message implements Serializable {
    private String message;
    private int UID;
    private int type; //0, 1, 2, 3
    private String name;

    public Message(String message, String name){
        this.message = name + ": " + message;
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public int getUID() {
        return UID;
    }

    public void setType(int type){
        this.type = type;
    }

    public int getType(){
        return type;
    }

    public int setUID (int count){
        UID = name.hashCode() + count;
        count++;
        return count;
    }


    public boolean equals(Message message){
        return message.getUID() == UID;
    }
}
