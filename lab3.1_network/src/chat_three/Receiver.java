package chat_three;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.Random;

public class Receiver implements Runnable{
    private Node node;
    private DatagramSocket socket;
    private Checker checker;
    private DatagramPacket datagramPacket;
    Receiver(Node node, Checker checker) {
        this.node = node;
        this.checker = checker;
        socket = node.getSocket();
    }

    @Override
    public void run() {
        Message message;
        Random random = new Random();
        int percent;
        try {
            while(true){
                synchronized (node) {
                    if (!node.isEnd()) {
                        message = receive();
                        //System.out.println(message.getType() + " : " + message.getMessage() + '\n');
                    } else {
                        return;
                    }
                }
                percent = Math.abs(random.nextInt()) % 100;
                if(percent < node.percent()){
                    continue;
                }
                switch (message.getType()){
                    case 2:
                        node.addChild(datagramPacket.getAddress(), datagramPacket.getPort());
                        node.addMessage(message, false);
                        sendFurther(message);
                        sendAnswer(message, 3);
                        break;
                    case 3:
                        synchronized (checker) {
                            checker.deleteMessage(message.getUID(), datagramPacket.getAddress(), datagramPacket.getPort());
                        }
                        break;
                    default:
                            break;

                }
            }
        } catch (ClassNotFoundException | IOException e){
            e.printStackTrace();
        }

    }

    private Message receive() throws IOException, ClassNotFoundException {
        datagramPacket = new DatagramPacket(new byte[2000], 0, 2000);
        socket.receive(datagramPacket);
        ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(datagramPacket.getData()));

        return (Message)objectInputStream.readObject();
    }

    private void sendAnswer(Message message, int type) throws IOException {
        message.setType(type);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(2000);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(message);
        DatagramPacket packet = new DatagramPacket(byteArrayOutputStream.toByteArray(),
                byteArrayOutputStream.toByteArray().length,
                datagramPacket.getAddress(),
                datagramPacket.getPort());
        socket.send(packet);
    }

    private void sendFurther(Message message) throws IOException {
        ArrayList<IpPortWrapper> children = node.getChildren();
        for(IpPortWrapper ipPortWrapper: children){
            if(!ipPortWrapper.getInetAddress().equals(datagramPacket.getAddress()) || !ipPortWrapper.getPort().equals(datagramPacket.getPort())){
                synchronized (checker) {
                    checker.addMessage(message, ipPortWrapper.getInetAddress(), ipPortWrapper.getPort());
                }
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(2000);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                objectOutputStream.writeObject(message);
                DatagramPacket packet = new DatagramPacket(byteArrayOutputStream.toByteArray(),
                        byteArrayOutputStream.toByteArray().length,
                        ipPortWrapper.getInetAddress(),
                        ipPortWrapper.getPort());
                socket.send(packet);
            }
        }

        if(!node.isRoot()){
            if(!node.getParentIP().equals(datagramPacket.getAddress()) || node.getParentPort() != datagramPacket.getPort()){
                synchronized (checker) {
                    checker.addMessage(message, node.getParentIP(), node.getParentPort());
                }
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(2000);
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                objectOutputStream.writeObject(message);
                DatagramPacket packet = new DatagramPacket(byteArrayOutputStream.toByteArray(),
                        byteArrayOutputStream.toByteArray().length,
                        node.getParentIP(),
                        node.getParentPort());
                socket.send(packet);
            }
        }
    }
}
