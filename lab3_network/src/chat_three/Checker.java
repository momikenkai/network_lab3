package chat_three;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.TimerTask;

public class Checker extends TimerTask {
    private Node node;
    private ArrayList<Packet> messages;
    private ArrayList<IpPortWrapper> numOfResend;

    public Checker(Node node) {
        this.node = node;
        messages = new ArrayList<>();
        numOfResend = new ArrayList<>();
    }

    public void addMessage(Message message, InetAddress inetAddress, int port) {
        messages.add(new Packet(inetAddress, port, message));
    }

    public void deleteMessage(int UID, InetAddress inetAddress, int port) {
        for (Packet packet : messages) {
            if (packet.getInetAddress().equals(inetAddress) && packet.getPort().equals(port) && packet.getMessage().getUID() == UID) {
                messages.remove(packet);
                break;
            }
        }

        for (IpPortWrapper ipPortWrapper : numOfResend) {
            if (ipPortWrapper.getInetAddress().equals(inetAddress) && ipPortWrapper.getPort().equals(port)) {
                ipPortWrapper.resetNumOfResend();
            }
        }

    }

    @Override
    public void run() {
        synchronized (this) {
            try {
                for (Packet packet : messages) {
                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(2000);
                    ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
                    objectOutputStream.writeObject(packet.getMessage());
                    DatagramPacket datagramPacket = new DatagramPacket(byteArrayOutputStream.toByteArray(),
                            byteArrayOutputStream.toByteArray().length,
                            packet.getInetAddress(),
                            packet.getPort());
                    node.getSocket().send(datagramPacket);
                    for (IpPortWrapper ipPortWrapper : numOfResend) {
                        if (ipPortWrapper.getInetAddress().equals(packet.getInetAddress()) && ipPortWrapper.getPort().equals(packet.getPort())) {
                            ipPortWrapper.incrementResend();
                        }
                    }
                }
                for (IpPortWrapper ipPortWrapper : numOfResend) {
                    if (ipPortWrapper.getNumOfResend() > 6) {
                        node.breakRelation(ipPortWrapper.getInetAddress(), ipPortWrapper.getPort());
                        numOfResend.remove(ipPortWrapper);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
