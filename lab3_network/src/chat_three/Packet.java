package chat_three;

import java.net.InetAddress;

public class Packet {
    private IpPortWrapper ipPortWrapper;
    private Message message;


    public Packet(InetAddress inetAddress, Integer port, Message message){
        this.message = message;
        ipPortWrapper = new IpPortWrapper(inetAddress, port);
    }

    public InetAddress getInetAddress() {
        return ipPortWrapper.getInetAddress();
    }

    public Integer getPort() {
        return ipPortWrapper.getPort();
    }

    public Message getMessage() {
        return message;
    }
}
