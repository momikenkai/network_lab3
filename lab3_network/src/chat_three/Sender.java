package chat_three;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Timer;

public class Sender implements Runnable{
    private int count = 2;
    private Node node;
    private Checker checker;
    private DatagramSocket socket;
    private Timer timer;
    Sender(Node node, Checker checker, Timer timer) {
        this.timer = timer;
        this.node = node;
        this.checker = checker;
        socket = node.getSocket();
    }

    @Override
    public void run() {
        try {
            Scanner scanner = new Scanner(System.in);
            Message message;
            ArrayList<IpPortWrapper> children = node.getChildren();
            if (!node.isRoot()) {
                message = new Message("Hi dad", node.getNodeName());
                count = message.setUID(count);
                message.setType(0);
                send(message, node.getParentIP(), node.getParentPort());
                synchronized (checker) {
                    checker.addMessage(message, node.getParentIP(), node.getParentPort());
                }
            }
            while (true) {
                String msg = scanner.nextLine();
                if(msg.equals("exit")){
                    node.setEnd();
                    node.printNodeMessages();
                    timer.cancel();
                    return;
                }
                message = new Message(msg, node.getNodeName());
                node.addMessage(message, true);
                count = message.setUID(count);
                message.setType(2);
                if (node.isParent()) {
                    send(message, node.getParentIP(), node.getParentPort());
                    synchronized (checker) {
                        checker.addMessage(message, node.getParentIP(), node.getParentPort());
                    }
                }
                for(IpPortWrapper ipPortWrapper : children){
                    send(message, ipPortWrapper.getInetAddress(), ipPortWrapper.getPort());
                    synchronized (checker) {
                        checker.addMessage(message, ipPortWrapper.getInetAddress(), ipPortWrapper.getPort());
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void send(Message message, InetAddress inetAddress, int port) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(2000);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(message);
        DatagramPacket packet = new DatagramPacket(byteArrayOutputStream.toByteArray(),
                byteArrayOutputStream.toByteArray().length,
                inetAddress,
                port);
        socket.send(packet);
    }
}
