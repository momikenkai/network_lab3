package chat_three;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Timer;


public class Main {
    public static void main(String[] args){
        if(args.length != 5 && args.length != 3){
            System.out.println("Wrong arguments\n " +
                    "Please specify node name, loss percentage, port, parent ip and parent port.\n");
            return;
        }

        try{
            Node node = new Node(args);
            Checker checker = new Checker(node);
            Timer timer = new Timer(true);
            timer.schedule(checker, 3000, 3000);
            new Thread(new Sender(node, checker, timer)).start();
            new Thread(new Receiver(node, checker)).start();
        } catch (SecurityException e){
            System.out.println("Wrong argument format");
        } catch (SocketException e){
            e.printStackTrace();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }


    }
}
